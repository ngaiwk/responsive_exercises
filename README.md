This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



The application show how we can control responsive web by using
 - css ( like @media, flexbox, '!important' when deal with import chart from library like echarts )
 - some coding skill for control the resize of screen

![Desktop size #1](img/DesktopTop.png)
![Desktop size #2](img/DesktopBottom.png)
![IPad size #1](img/IPadTop.png)
![IPhone size #1](img/IphoneTop.png) 
![IPhone size #2](img/IphoneBottom.png)  
![Small screen size #1](img/S_Top.png)
![Small screen size #2](img/S_Bottom.png)

