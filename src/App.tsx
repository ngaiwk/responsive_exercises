import React from 'react';
import './App.css';
import DoughNutChart from './component/doughnutchart'
import BarLineChart from './component/barlinechart'
import HorizonChart from './component/horizonbar'
import { DoughNutA, DoughNutB, DoughNutC, DoughNutD, BarCharData, horizontalData } from './table'
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';


function App() {

  return (
    <div className="container">
      <div className="App">
        <Container >
          <div className="rowBar">
            <Row>
              <div className="coldive">
                <DoughNutChart doughnut={DoughNutA} />
              </div>
              <div className="coldive">
                <DoughNutChart doughnut={DoughNutB} />
              </div>
              <div className="coldive">
                <DoughNutChart doughnut={DoughNutC} />
              </div>
              <div className="coldive">
                <DoughNutChart doughnut={DoughNutD} />
              </div>
            </Row>
          </div>
          <div className="rowBar">
            <BarLineChart barLineData={BarCharData} />
          </div>
          <div className="rowBar">
            <HorizonChart horizontal={horizontalData} />
          </div>
        </Container>
      </div>
    </div>
  );
}

export default App;
