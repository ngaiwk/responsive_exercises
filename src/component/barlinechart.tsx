import React from 'react';
import ReactEcharts from 'echarts-for-react';

interface IChartProps {
    barLineData:any
}

function BarLineChart (props:IChartProps){
        const option = {
                grid: {
                    left:'20%',
                    top: '10%',
                    right: '15%',
                    bottom: '30%',
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: [{name:'inventory',icon:'rect'}
                    , { name:'Days of inventory',icon:'line'}],
                    bottom : 20,
                    //icon : 'line'
                    //selectedMode: false,
                },
                xAxis: [
                    {
                        type: 'category',
                        //data: props.barLineData.name,
                        data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
                        axisPointer: {
                            type: 'shadow'
                        },
                        axisLabel: {
                            rotate: 45
                        },

                    }
                ],
                yAxis: [
                    {
                        nameLocation : 'middle',
                        nameTextStyle:{
                            padding:[0,0,20,0]
                        },
                        type: 'value',
                        name: 'Inventory Level',
                        min: 0,
                        max: 350,
                        interval: 50,
                        axisLabel: {
                            formatter: '{value}'
                        },
                 
                    },
                    {
                        nameLocation : 'middle',
                        nameTextStyle:{
                            padding:[10,0,0,0]
                        }, 
                        type: 'value',
                        name: 'Days of Inventory',
                        min: 0,
                        max: 70,
                        interval: 10,
                        axisLabel: {
                            formatter: '{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: 'inventory',
                        type: 'bar',
                        data:props.barLineData.inventory,
                        //data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
                        barWidth:20,
                        color : "#4472c4"
                    },
                    {
                        name: 'Days of inventory',
                        type: 'line',
                        yAxisIndex: 1,
                        showSymbol: false,
                        //hoverAnimation: false,
                        data:props.barLineData.daysOfInventory,
                        //data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                        color : "#ed823a"
                    }
                ]
        };




        return (
            <>
                <ReactEcharts option={option} />
                {/* <ReactEcharts option={option} opts={{ height: 280}}/> */}
            </>
        )

}

export default BarLineChart