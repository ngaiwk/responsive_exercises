import React from 'react';
import ReactEcharts from 'echarts-for-react';

interface IChartProps {
    doughnut:any
}

function DoughNutChart (props:IChartProps){
       // console.log(props.name)
        var option = {
            // grid: {
            //     //left: 5,
            //      //top:100 ,
            //     //right: 5,
            //     //bottom: 100
            // },

            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            title : {
                top:'5%' ,
                text:props.doughnut.topic[0],
                textStyle: {
                    fontSize:"15"
                },
                x: 'center'
            },
            graphic:{
                type:'text',
                id:'text1',
                right: 'center',
                left:'center',
                top: 'middle',
                bottom: 'middle',
                style: {
                    text: props.doughnut.data[1].value,
                    font: '6vw Microsoft YaHei'
                }
            },
            series: [
                {
                    name: props.doughnut.topic[0],
                    type: 'pie',
                    radius: ['55%', '70%'],
                    selectedMode: 'multiple',
                    selectedOffset :0,
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: false,
                            textStyle: {
                                // fontSize: '30',
                                fontWeight: 'bold'
                            },
                            radius : ['55%', '70%']
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    color:
                        ['#4472c4','#ed823a'],
                    data: props.doughnut.data
                }
            ]
            
        };

        


        return (
            <ReactEcharts className = "pie_control" option={option} />
            )
}

export default DoughNutChart