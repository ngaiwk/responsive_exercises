import React from 'react';
import ReactEcharts from 'echarts-for-react';
import useWindowDimensions from '../utils/useWindowDimensions';

interface IChartProps {
    horizontal: any
}


function HorizonChart(props: IChartProps) {
    // console.log(props.name)
    const { width } = useWindowDimensions();

    //console.log(width);
    const getYAxis = () => {
        //console.log(width);
        return width >= 500 ?
        ({
            type: 'category',
            data: props.horizontal.name,
        }) :
        ({
            type: 'category',
        })
    }

    const option = {
        graphic: {
            type: 'text',
            id: 'text1',
            right: 'center',
            left: 'center',
            //top: 'middle',
            bottom: '5%',
            style: {
                text: 'Day(s)',
                font: '0.9em Microsoft YaHei'
            }
        },
        // legend: {
        //     data: ['直接访问', ]
        // },
        grid: {
            left:'5%',
            top: '5%',
            right: '10%',
            bottom: '10%',
            containLabel: true
        },
        xAxis: {
            type: 'value',
            interval: 5,
        },
        yAxis: getYAxis(),
        series: [
            {
                name: '直接访问',
                type: 'bar',
                stack: '总量',
                label: {
                    show: true,
                    //position: 'insideRight'  
                    position: 'right',
                    //colour: '#000',
                    //colour: '#1234',
                    //color : "#5c5c5c"
                    color: "#000000"

                },
                data: props.horizontal.data,
                //data: [0.03, 0.8, 0.5, 0.3, 0.4, 0.7, 14]
                color: "#4472c4",
                barWidth: 20,
            },
        ]
    };

    return (

        <>

                <div className='barright'><ReactEcharts option={option} className="bar_control" /></div>
 
        </>

    )

}

export default HorizonChart