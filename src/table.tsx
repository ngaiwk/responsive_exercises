
export const DoughNutA={

       topic : ['Purchase Order'],
       data : [
        {value: 6000, name: 'ordered'},
        {value: 98, name: 'delivered'}
       ]
    }

    
    export const DoughNutB={

        topic : ['Outbound Shipment'],
        data : [
         {value: 80, name: 'airport'},
         {value: 40, name: 'delivered'}
        ]
     }
     export const DoughNutC={

        topic : ['Inbound Shipment'],
        data : [
         {value: 60, name: 'airport'},
         {value: 33, name: 'warehouse'}
        ]
     }
     export const DoughNutD={

        topic : ['Sales Order'],
        data : [
         {value: 2, name: 'customer order'},
         {value: 4, name: 'delivered',}
        ]
     }

export const BarCharData={
      name : ['Product 1','Product 2','Product 3','Product 4','Product 5','Product 6','Product 7','Product 8','Product 9','Product 10'],
      inventory:[150,50,300,230,40,30,20,40,30,20],
      daysOfInventory:[56,48,53,57,56,58,60,38,45,38]
   }

export const horizontalData={
      name : [
      'Confirmation to Exit Factory',
      'Exit Factory to Arrive Outbound Airport',
      'Arrive Outbound Airport to Arrive inbound',
      'Arrive inbound Airport to warehouse',
      'Customer Order to pick',
      'Pick to Arrive Customer address',
      'Arrive Customer address to Goods Receipt'
      ],
      data:[14,0.7,0.4,0.3,0.5,0.8,0.03]
      //data:[0.03, 0.8, 0.5, 0.3, 0.4, 0.7, 14]
   }
